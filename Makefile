#=====================================
# OpenWrt Makefile
#=====================================

include $(TOPDIR)/rules.mk

PKG_NAME:=wsock
PKG_VERSION:=1.1.1
PKG_RELEASE:=1

PKG_BUILD_DIR:= $(BUILD_DIR)/$(PKG_NAME)
#PKG_BUILD_DEPENDS:=lib
 
#include $(INCLUDE_DIR)/kernel.mk
include $(INCLUDE_DIR)/package.mk


define Package/wsock
	SECTION:=utils
	CATEGORY:=Utilities
	TITLE:=Websocket
	MAINTAINER:=Pavlo Bezhan <pbezhan@gmail.com>
	DEPENDS:=+libpthread
endef

TARGET_CPPFLAGS := \
	-D_GNU_SOURCE \
	-I$(PKG_BUILD_DIR) \
	$(TARGET_CPPFLAGS) \
	-I$(LINUX_DIR)/user_headers/include


define Package/wsock/description
	Socket server
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)/
endef


define Build/Compile
	CFLAGS="$(TARGET_CPPFLAGS) $(TARGET_CFLAGS)" \
	$(MAKE) -C $(PKG_BUILD_DIR) \
		$(TARGET_CONFIGURE_OPTS) \
		LIBS="$(TARGET_LDFLAGS)"
endef



define Package/wsock/install
	$(INSTALL_DIR) $(1)/bin
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/wsock $(1)/
endef

$(eval $(call BuildPackage,wsock))

