#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define BUF_SIZE 256
#define BUF_SIZE_SMALL 128
 

#define COM_STATUS 	"GETSTATUS"
#define COM_SETTEL 	"SETALARMCELL"
#define COM_ALARM 	"ALARM"


#define FILE_ALARMNET 	"/tmp/ig_alarmnet.dat"
#define FILE_VIDEONET 	"/tmp/ig_videonet.dat"
#define FILE_AJAX	  	"/tmp/ig_ajax.dat"
#define FILE_CELL	  	"/var/ig_cell.dat"
#define FILE_CELL_NEW	"/tmp/ig_cell.new"
#define FILE_GPS		"/var/ig_gps.dat"
#define FILE_ALARM 		"/var/ig_alarm.dat"

void error(const char *msg) {
    perror(msg);
    exit(1);
}

char *trimwhitespace(char *str)
{
  char *end;

  // Trim leading space
  while(isspace((unsigned char)*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace((unsigned char)*end)) end--;

  // Write new null terminator
  *(end+1) = 0;

  return str;
}

/*
 *		E-Plus
 *		-73
 *		connected
 *		registered (home)
 *		LTE
 *  
 */
 
int getfiledata( char *buf, char *filename)
{
	FILE *fp;
	
	memset( buf, 0, BUF_SIZE_SMALL);
	if ((fp = fopen( filename, "r"))==NULL) {
		strcpy( buf, "--\n");
		return -1;
	}
	int rbytes= fread( buf, sizeof(char), BUF_SIZE_SMALL, fp);
	fclose(fp);	
	
	return 0;
	
} 
/* 
int getvideonet( char *buf)
{
	FILE *fp;
	
	memset( buf, 0, BUF_SIZE_SMALL);
	if ((fp = fopen(FILE_VIDEONET, "r"))==NULL) {
		strcpy( buf, "--\n--\n--\n--\n--\n");
		return -1;
	}
	int rbytes= fread( buf, sizeof(char), BUF_SIZE_SMALL, fp);
	fclose(fp);	
	
	return 0;
	
}

int getalarmnet( char *buf)
{
	FILE *fp;
	
	memset( buf, 0, BUF_SIZE_SMALL);
	if ((fp = fopen(FILE_ALARMNET, "r"))==NULL) {
		strcpy( buf, "--\n--\n--\n");
		return -1;
	}
	fread( buf, sizeof(char), BUF_SIZE_SMALL, fp);
	fclose(fp);	
	
	return 0;
}

int getajax( char *buf)
{
	FILE *fp;
	
	memset( buf, 0, BUF_SIZE_SMALL);
	if ((fp = fopen(FILE_AJAX, "r"))==NULL) {
		strcpy( buf, "--\n");
		return -1;
	}
	fread( buf, sizeof(char), BUF_SIZE_SMALL, fp);
	fclose(fp);	
	
	return 0;
}
*/
int sethandy ( char *in, char *buf)
{
	
	char* token;
	char* string;
	char* tofree;

	memset( buf, 0, BUF_SIZE_SMALL);
	
	string = strdup( in);

	if (string != NULL) 
	{
		tofree = string;
		while ((token = strsep(&string, " ")) != NULL)
		{
			if ( strcmp(token, COM_SETTEL) != 0 )
			{
				strcat( buf, token);
				strcat( buf, "\n");
			}
		}
		free(tofree);
		
		FILE *fp;
		if ((fp = fopen(FILE_CELL, "w"))==NULL) 
		{
			strcpy( buf, "Error: file not open\n");
			return -1;
		}
		fwrite( buf, sizeof(char), strlen(buf), fp);
		fclose(fp);			

		FILE *fpn;
		if ((fpn = fopen(FILE_CELL_NEW, "w"))==NULL) 
		{
			strcpy( buf, "Error: lock file not open\n");
			return -1;
		}
		//fwrite( buf, sizeof(char), strlen(buf), fp);
		fclose(fpn);			
		
	}	
	return 0;
	
}

int setalarm ( char *in, char *buf)
{
	
	char* token;
	char* string;
	char* tofree;

	memset( buf, 0, BUF_SIZE_SMALL);
	
	string = strdup( in);

	if (string != NULL) 
	{
		tofree = string;
		while ((token = strsep(&string, " ")) != NULL)
		{
							printf("token=%s\n", token);

			if ( strcmp(token, COM_ALARM) != 0 )
			{
				strcat( buf, token);
				strcat( buf, "\n");
			}
		}
		free(tofree);
		
		FILE *fp;
		if ((fp = fopen(FILE_ALARM, "w"))==NULL) 
		{
			strcpy( buf, "Error: file not open\n");
			return -1;
		}
		fwrite( buf, sizeof(char), strlen(buf), fp);
		fclose(fp);			
		
	}	
	return 0;
	
}

int getstatus( char *buf)
{
    char buf1[BUF_SIZE_SMALL];

	
	// Video SIM
	getfiledata( buf1, FILE_VIDEONET);
    strcpy( buf, buf1);

	// Alarm SIM
	getfiledata( buf1, FILE_ALARMNET);
    strncat( buf, "---\n", strlen("---\n"));
    strncat( buf, buf1, strlen(buf1));
    
    // Ajax Status
    getfiledata( buf1, FILE_AJAX);
    strncat( buf, "---\n", strlen("---\n"));
    strncat( buf, buf1, strlen(buf1));

	// Handy Nummern
    getfiledata( buf1, FILE_CELL);
    strncat( buf, "---\n", strlen("---\n"));
    strncat( buf, buf1, strlen(buf1));
    
    // Alarm Status
    getfiledata( buf1, FILE_ALARM);
    strncat( buf, "---\n", strlen("---\n"));
    strncat( buf, buf1, strlen(buf1));

	// GPS 
	getfiledata( buf1, FILE_GPS);
    strncat( buf, "---\n", strlen("---\n"));
    strncat( buf, buf1, strlen(buf1));
	
    
    return 0;
}

int main(int argc, char *argv[]) {
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[BUF_SIZE];
    char buf[BUF_SIZE];
    struct sockaddr_in serv_addr, cli_addr;
    int n;
    if (argc < 2) {
         fprintf(stderr, "ERROR, no port provided\n");
         exit(1);
    }
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
         error("ERROR opening socket");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = atoi(argv[1]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("ERROR on binding");
    listen(sockfd, 5);
    clilen = sizeof(cli_addr);
    //Below code is modified to handle multiple clients using fork
    //------------------------------------------------------------------
    int pid;
    while (1) {
         newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
         if (newsockfd < 0)
                error("ERROR on accept");
         //fork new process
         pid = fork();
         if (pid < 0) {
              error("ERROR in new process creation");
         }
         if (pid == 0) {
            //child process
            close(sockfd);
            //do whatever you want
            bzero(buffer, BUF_SIZE);
            n = read(newsockfd, buffer, BUF_SIZE-1);
            if (n < 0)
                error("ERROR reading from socket");
            printf("Here is the message: [%s]\n", trimwhitespace(buffer));
            int rc = -1;
            if ( strncmp( trimwhitespace(buffer), COM_STATUS, strlen(COM_STATUS)) == 0)
            {
                rc = getstatus( buf) ;
            }
            else if ( strncmp( trimwhitespace(buffer), COM_SETTEL, strlen(COM_SETTEL) ) == 0)
            {
                rc = sethandy( buffer , buf) ;
            }
            else if ( strncmp( trimwhitespace(buffer), COM_ALARM, strlen(COM_ALARM) ) == 0)
            {
				printf("SETALARM\n");
                rc = setalarm( buffer , buf) ;
            }
           
            if ( rc == 0 )   
            {       
                n = write(newsockfd, buf, strlen(buf));
                if (n < 0)
                    error("ERROR writing to socket");
            }
            else
            {
                n = write(newsockfd, "command unknown\n", strlen("command unknown\n"));
                if (n < 0)
                    error("ERROR writing to socket");
            }
            close(newsockfd);
          } else {
             //parent process
             close(newsockfd);
          }
    }
    //-------------------------------------------------------------------
   return 0;
}
